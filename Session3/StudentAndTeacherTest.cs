using System;

namespace Session3
{
    /// <summary>
    /// Class StudentAndTeacherTest for test
    /// </summary>
    class StudentAndTeacherTest
    {
        static void Main(string[] args)
        {
            Person newPerson = new Person();
            newPerson.Say("Hello");

            Student newStudent = new Student();
            newStudent.SetAge(21);
            newStudent.GotoClasses();
            newStudent.ShowAge();

            Teacher newTeacher = new Teacher();
            newTeacher.SetAge(30);
            newTeacher.Say("Hello");
            newTeacher.Explain();

            Console.ReadKey();
        }
    }
}
