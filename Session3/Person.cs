﻿using System;

namespace Session3
{
    /// <summary>
    /// Person with some common properties and methods
    /// </summary>
    class Person
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        protected int _age;

        /// <summary>
        /// Method person says
        /// </summary>
        /// <param name="Text">Text</param>
        public void Say(string Text)
        {
            Console.WriteLine(Text);
        }

        /// <summary>
        /// Method set age for person
        /// </summary>
        /// <param name="Age">Age of Person</param>
        public void SetAge(int Age)
        {
            _age = Age;
        }
    }
}
