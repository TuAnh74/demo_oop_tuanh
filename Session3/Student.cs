﻿using System;

namespace Session3
{
    /// <summary>
    /// Child of class person
    /// </summary>
    class Student : Person
    {
        /// <summary>
        /// Method student go to class in the Student class 
        /// </summary>
        public void GotoClasses()
        {
            Console.WriteLine("I'm going to class");
        }

        /// <summary>
        /// Method show student age in the Student class
        /// </summary>
        public void ShowAge()
        {
            Console.WriteLine("My age is: {0} years old", _age);
        }
    }
}
