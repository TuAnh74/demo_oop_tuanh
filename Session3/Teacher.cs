﻿using System;

namespace Session3
{
    /// <summary>
    /// Child of class person
    /// </summary>
    class Teacher : Person
    {
        private string subject { get; set; }

        /// <summary>
        /// Method in Teacher class for teacher explain
        /// </summary>
        public void Explain()
        {
            Console.WriteLine("Explanation begins");
        }
    }
}
